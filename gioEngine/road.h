#pragma once

#include "tile.h"

//Used for road tiles
class Road : public Tile
{
	public:
		Road(Texture* t);

		void update() override;

		~Road();

	private:
		bool sides[4];
};

