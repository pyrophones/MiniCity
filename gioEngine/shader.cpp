#include "shader.h"

//Ctor
Shader::Shader()
{

}

//Loads the shaders
bool Shader::loadShaders(const GLchar* vertLoc, const GLchar* fragLoc)
{
	GLint success;
	GLchar infoLog[512];
	GLuint vert, frag;

	vert = loadShader(vertLoc, GL_VERTEX_SHADER);
	if (vert == 0)
		return false;

	frag = loadShader(fragLoc, GL_FRAGMENT_SHADER);
	if (frag == 0)
		return false;

	this->program = glCreateProgram();
	glAttachShader(this->program, vert);
	glAttachShader(this->program, frag);
	glLinkProgram(this->program);

	glGetProgramiv(this->program, GL_LINK_STATUS, &success);

	if (!success)
	{
		glGetProgramInfoLog((this->program), 512, nullptr, infoLog);
		std::cout << "Error: Program linking failed: " << infoLog << std::endl;
		return false;
	}

	else
		return true;
}

//Helper methos for shader loading
GLuint Shader::loadShader(const GLchar* file, GLenum fileType)
{
	std::ifstream stream;
	size_t size;
	GLchar* src;

	stream.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	stream.open(file, std::ios::binary);

	if (!stream.is_open())
	{
		std::cout << "Failed to open vertex shader!" << std::endl;
		return 0;
	}

	stream.seekg(0, std::ios::end);
	size = stream.tellg();
	stream.seekg(0, std::ios::beg);

	src = new GLchar[size + 1];
	stream.read(src, size);
	src[size] = 0;

	stream.close();

	GLint success;

	GLchar infoLog[512];

	GLuint shad = glCreateShader(fileType);
	glShaderSource(shad, 1, &src, nullptr);
	glCompileShader(shad);
	delete src;

	glGetShaderiv(shad, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(shad, 512, nullptr, infoLog);
		std::cout << "Error during shader compilation: " << infoLog << std::endl;
		glDeleteShader(shad);
		return 0;
	}

	else
		return shad;
}

//Getter for shader program
GLuint Shader::getProgram() const
{
	return this->program;
}

//Uses the current shader
void Shader::use()
{
	glUseProgram(this->program);
}

//Dtor
Shader::~Shader()
{
	glDeleteProgram(this->program);
}