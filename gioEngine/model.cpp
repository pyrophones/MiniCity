#include "model.h"
#include "engine.h"

//Ctor
Model::Model()
{

}

//Ctor that reads a model
Model::Model(const GLchar* path)
{
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	this->readModel(path);
}

//Reads a model
void Model::readModel(const GLchar* path)
{
	std::ifstream stream;
	size_t size;
	GLchar* src;

	stream.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	stream.open(path, std::ios::binary);

	if (!stream.is_open())
	{
		std::cout << "Failed to open model!" << std::endl;
	}

	stream.seekg(0, std::ios::end);
	size = stream.tellg();
	stream.seekg(0, std::ios::beg);

	src = new GLchar[size + 1];
	stream.read(src, size);
	src[size] = 0;

	stream.close();

	std::stringstream ss(src);
	delete[] src;

	glm::vec3 tmpVerts;
	std::vector<glm::vec3> verts;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> norms;
	std::vector<glm::vec3> indicies;

	GLchar tChar;
	std::string tmpString;

	while (ss >> tmpString)
	{
		if (tmpString[0] == 'v')
		{
			switch (tmpString[1])
			{
				case '\0':
				{
					ss >> tmpVerts.x >> tmpVerts.y >> tmpVerts.z;
					verts.push_back(tmpVerts);

					//Check for max
					if (glm::abs(tmpVerts.x) > glm::abs(this->max.x))
						this->max.x = glm::abs(tmpVerts.x);

					if (glm::abs(tmpVerts.y) > glm::abs(this->max.y))
						this->max.y = glm::abs(tmpVerts.y);

					if (glm::abs(tmpVerts.z) > glm::abs(this->max.z))
						this->max.z = glm::abs(tmpVerts.z);

					break;
				}

				case 't':
				{
					ss >> tmpVerts.x >> tmpVerts.y;
					uvs.push_back((glm::vec2)tmpVerts);
					break;
				}

				case 'n':
				{
					ss >> tmpVerts.x >> tmpVerts.y >> tmpVerts.z;
					norms.push_back(tmpVerts);
					break;
				}
			}
		}

		else if (tmpString[0] == 'f' && tmpString[1] == '\0')
		{
			ss >> tmpVerts.x >> tChar >> tmpVerts.y >> tChar >> tmpVerts.z;
			tmpVerts.x--;
			tmpVerts.y--;
			tmpVerts.z--;
			indicies.push_back(tmpVerts);

			ss >> tmpVerts.x >> tChar >> tmpVerts.y >> tChar >> tmpVerts.z;
			tmpVerts.x--;
			tmpVerts.y--;
			tmpVerts.z--;
			indicies.push_back(tmpVerts);

			ss >> tmpVerts.x >> tChar >> tmpVerts.y >> tChar >> tmpVerts.z;
			tmpVerts.x--;
			tmpVerts.y--;
			tmpVerts.z--;
			indicies.push_back(tmpVerts);
		}
	}

	ss.str("");
	ss.clear();

	Vertex tmp;

	for (GLuint i = 0; i < indicies.size(); i++)
	{
		tmp.vert.x = verts[indicies[i].x].x;
		tmp.vert.y = verts[indicies[i].x].y;
		tmp.vert.z = verts[indicies[i].x].z;

		tmp.uv.x = uvs[indicies[i].y].x;
		tmp.uv.y = uvs[indicies[i].y].y;

		tmp.norm.x = norms[indicies[i].z].x;
		tmp.norm.y = norms[indicies[i].z].y;
		tmp.norm.z = norms[indicies[i].z].z;

		this->vert.push_back(tmp);
	}

	count = indicies.size();
}

//Buffer the model
bool Model::bufferModel()
{
	glBindVertexArray(this->vao);
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	glBufferData(GL_ARRAY_BUFFER, this->count * sizeof(Vertex), &vert[0], GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(GLfloat)));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(5 * sizeof(GLfloat)));

	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	texLoc = glGetUniformLocation(Engine::shaderMan.getProgram(), "tex");
	transLoc = glGetUniformLocation(Engine::shaderMan.getProgram(), "transform");
	colorLoc = glGetUniformLocation(Engine::shaderMan.getProgram(), "texColor");
	
	return true;
}

//render the model
void Model::render(const glm::mat4 &transMat)
{
	glUniform1i(texLoc, 0);
	glUniformMatrix4fv(transLoc, 1, GL_FALSE, glm::value_ptr(transMat));
	glUniform3fv(colorLoc, 1, glm::value_ptr(color));

	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, count);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

//destorys addition GL things
void Model::destroy()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	Model::~Model();
}

//Dtor
Model::~Model()
{

}
