#include "time.h"

GLfloat Time::deltaTime = 0;
GLfloat Time::time = 0;
GLfloat Time::curTime = 0;

//Calculates dT
void Time::calcDeltaTime(GLfloat newTime)
{
	time = curTime;
	curTime = newTime;
	deltaTime = curTime - time;
}