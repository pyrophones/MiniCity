#pragma once

#include <GL/glew.h>
#include <iostream>
#include <fstream>
#include <string>

//Loads a shader
class Shader
{
	private:
		GLuint program;
		GLuint loadShader(const GLchar* file, GLenum shaderType);

	public:
		Shader();
		GLuint getProgram() const;
		void use();
		bool loadShaders(const GLchar* vertLoc, const GLchar* fragLoc);
		~Shader();
};