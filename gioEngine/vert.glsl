#version 430 core

layout (location = 0) in vec3 vert;
layout (location = 1) in vec2 uv;
layout (location = 2) in vec3 norm;

out vec3 VERT;
out vec2 UV;
out vec3 NORM;

uniform vec3 pos;
uniform vec3 scale;
uniform mat4 transform;
uniform mat4 camView;

void main()
{
    gl_Position = camView * (transform * vec4(vert, 1.0));
	VERT = (transform * vec4(vert, 0.0)).xyz;
	UV = uv;
	NORM = (transpose(inverse(transform)) * vec4(norm, 0.0)).xyz;
}