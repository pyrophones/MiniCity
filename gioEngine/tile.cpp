#include "tile.h"

//Ctor
Tile::Tile(Texture* tex) : Object(&AssetLoader::loadedModels["quad"], tex)
{
	zCollisions = false;
	max = glm::vec3(max.x - 0.1, max.y - 0.1, max.z - 0.1);
}

//Update override. Calls objects update
void Tile::update()
{
	Object::update();

	//Checks for tile collisions
	for (auto i : objectBatch)
	{
		colliding = false;

		if (i->tag != "res" && i->tag != "com" && i->tag != "ind" && i->tag != "road")
			continue;

		if (this->collidesWith(i))
		{
			colliding = true;
			break;
		}
	}
}

//Dtor
Tile::~Tile()
{
	Object::~Object();
}
