#include "object.h"

std::vector<Object*> Object::objectBatch; // std::vector of the created objects

//Ctor
Object::Object()
{
	this->tag = "Default";
	this->rigidBody.setTransform(this->transform);
	this->collider = Collider::ColliderLess;
}

//Ctor that load a model
Object::Object(Model* m)
{
	this->tag = "Default";
	this->mod = m;
	this->rigidBody.setTransform(this->transform);
	this->collider = Collider::ColliderLess;

	objectBatch.push_back(this);
}

//Ctor that loads a texture
Object::Object(Texture* t)
{
	this->tag = "Default";
	this->tex = t;
	this->rigidBody.setTransform(this->transform);
	this->collider = Collider::ColliderLess;

	objectBatch.push_back(this);
}

//Ctor that loads a model and texure
Object::Object(Model* m, Texture* t)
{
	this->tag = "Default";
	this->mod = m;
	this->tex = t;
	this->rigidBody.setTransform(this->transform);
	this->collider = Collider::ColliderLess;

	max = mod->max;
	objectBatch.push_back(this);
}

//Update calls transform and rigidbody's update methods
void Object::update()
{
	transform.update();
	rigidBody.update();
}

//Render binds the texture and calls models render method
void Object::render()
{
	tex->bindTexture();
	mod->render(this->transform.transMat);
}

//Collision detection
bool Object::collidesWith(const Object* obj)
{
	//Shouldn't collide with itseld
	if (this == obj)
		return false;

	//If there are no colliders
	if (this->collider == Collider::ColliderLess || obj->collider == Collider::ColliderLess)
	{
		return false;
	}

	//Sphere to Sphere
	if (this->collider == Collider::Sphere && obj->collider == Collider::Sphere)
	{
		if (glm::pow(this->transform.pos.x - obj->transform.pos.x, 2) + glm::pow(this->transform.pos.y - obj->transform.pos.y, 2) + glm::pow(this->transform.pos.z - obj->transform.pos.z, 2) >
			glm::pow((this->max.x + obj->max.x), 2))
			return false;
	}

	//Sphere to AABB
	else if ((this->collider == Collider::AABB && obj->collider == Collider::Sphere) ||
			 (this->collider == Collider::Sphere && obj->collider == Collider::AABB))
	{
		GLfloat dis = 0;

		if(this->transform.pos.x > obj->transform.pos.x)
		{
			dis += glm::pow(this->transform.pos.x - obj->transform.pos.x, 2);
		}

		else if (this->transform.pos.x  < obj->transform.pos.x)
		{
			dis += glm::pow(obj->transform.pos.x - this->transform.pos.x, 2);
		}

		if (this->transform.pos.y > obj->transform.pos.y)
		{
			dis += glm::pow(this->transform.pos.y - obj->transform.pos.y, 2);
		}

		else if (this->transform.pos.y < obj->transform.pos.y)
		{
			dis += glm::pow(obj->transform.pos.y - this->transform.pos.y, 2);
		}

		if (this->transform.pos.z > obj->transform.pos.z)
		{
			dis += glm::pow(this->transform.pos.z - obj->transform.pos.z, 2);
		}

		else if (this->transform.pos.z < obj->transform.pos.z)
		{
			dis += glm::pow(obj->transform.pos.z - this->transform.pos.z, 2);
		}

		if (this->collider == Collider::Sphere)
		{
			if (dis > glm::pow(this->max.x * this->transform.scale.x, 2))
				return false;
		}

		else
		{
			if (dis > glm::pow(obj->max.x * obj->transform.scale.x, 2))
				return false;
		}
		
	}

	//AABB to AABB
	else if (this->collider == Collider::AABB && obj->collider == Collider::AABB)
	{
		if (glm::abs(this->transform.pos.x - obj->transform.pos.x) > ((this->max.x * this->transform.scale.x) + (obj->max.x * obj->transform.scale.x)))
			return false;
		
		if (glm::abs(this->transform.pos.y - obj->transform.pos.y) > ((this->max.y * this->transform.scale.y) + (obj->max.y * obj->transform.scale.y)))
			return false;
		
		if (zCollisions)
		{
			if (glm::abs(this->transform.pos.z - obj->transform.pos.z) > ((this->max.z * this->transform.scale.z) + (obj->max.z * obj->transform.scale.z)))
				return false;
		}
	}

	return true;
}

//Dtor
Object::~Object()
{

}
