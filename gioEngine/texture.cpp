#include "texture.h"

//Ctor
Texture::Texture()
{
	this->baseScale = 1;
}

//Ctor that loads a texture
Texture::Texture(GLchar* path)
{
	loadTexture(path);
}

//Texture loading method
void Texture::loadTexture(const GLchar* path)
{
	glGenTextures(1, &this->texture);
	glBindTexture(GL_TEXTURE_2D, this->texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	FIBITMAP* img = FreeImage_Load(FreeImage_GetFileType(path, 0), path);
	if (img == 0)
		std::cout << "Image could not be loaded" << std::endl;

	FIBITMAP* bmp32 = FreeImage_ConvertTo32Bits(img);
	if (bmp32 == 0)
		std::cout << "Image could not be converted" << std::endl;

	FreeImage_Unload(img);
	
	this->width = FreeImage_GetWidth(bmp32);
	this->height = FreeImage_GetHeight(bmp32);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, this->width, this->height, 0, GL_BGRA, GL_UNSIGNED_BYTE, (GLvoid*)FreeImage_GetBits(bmp32));
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
	FreeImage_Unload(bmp32);
}

//Gets the current texture
GLuint Texture::getTexture()
{
	return this->texture;
}

//Binds the current texture
void Texture::bindTexture()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->texture);
}

//Calls nessesary GL methods to get rid of the texture
void Texture::destroy()
{
	glDeleteTextures(1, &this->texture);
}

//Dtor
Texture::~Texture()
{

}
