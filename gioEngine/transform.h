#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/transform.hpp>

//Holds transform info
class Transform
{
	public:
		glm::mat4 transMat;
		glm::vec3 pos = glm::vec3();
		glm::vec3 scale = glm::vec3(1.0, 1.0, 1.0);
		glm::vec3 rot = glm::vec3(1.0, 1.0, 1.0);
		glm::vec3 front;
		glm::vec3 right;
		glm::vec3 up;

		Transform();
		Transform(GLfloat x, GLfloat y, GLfloat z);
		Transform(glm::vec3 pos);
		virtual ~Transform();
		virtual void update();		
};

