#include "road.h"

//Ctor
Road::Road(Texture* t) : Tile(t)
{
	type = TileTypes::road;
}

//Update overide. Calls tiles update and determines where the road should face and what texture to sue
void Road::update()
{
	Tile::update();

	for (bool i : sides)
		i = false;

	glm::vec3 tmp = max;
	max = mod->max;
	max.x += 0.5;
	max.y += 0.5;
	max.z += 0.5;
	GLfloat count = 0;

	//Check if is next to any roads
	for (auto i : objectBatch)
	{
		if (collidesWith(i) && i->tag == "road" )
		{
			if (this->transform.pos.x == i->transform.pos.x)
			{
				if (this->transform.pos.y > i->transform.pos.y)
					sides[0] = true;
				else
					sides[2] = true;

				count++;
			}
			
			else if (this->transform.pos.y == i->transform.pos.y)
			{
				this->transform.rot = glm::vec3(0, 0, glm::radians(90.0f));

				if (this->transform.pos.x > i->transform.pos.x)
					sides[1] = true;
				else
					sides[3] = true;

				count++;
			}
		}
	}

	//Angled roads
	if (count == 4)
	{
		if ((sides[1] || sides[3]) && (!sides[0] && !sides[2]))
		{
			this->tex = &AssetLoader::loadedTextures["road"];
			this->transform.rot = glm::vec3(0, 0, glm::radians(90.0f));
		}

		else if (sides[0] && sides[1])
		{
			this->tex = &AssetLoader::loadedTextures["roadDiag"];
			this->transform.rot = glm::vec3(0, 0, glm::radians(-90.0f));
		}

		else if (sides[1] && sides[2])
		{
			this->tex = &AssetLoader::loadedTextures["roadDiag"];
			this->transform.rot = glm::vec3(0, 0, glm::radians(180.0f));
		}
		
		else if (sides[2] && sides[3])
		{
			this->tex = &AssetLoader::loadedTextures["roadDiag"];
			this->transform.rot = glm::vec3(0, 0, glm::radians(90.0f));
		}

		else if (sides[3] && sides[0])
		{
			this->tex = &AssetLoader::loadedTextures["roadDiag"];
			this->transform.rot = glm::vec3(0, 0, glm::radians(0.0f));
		}

		else
		{
			this->tex = &AssetLoader::loadedTextures["road"];
			this->transform.rot = glm::vec3(0, 0, glm::radians(0.0f));
		}
	}

	//T intersections
	else if (count == 6)
	{
		this->tex = &AssetLoader::loadedTextures["roadT"];

		if (sides[0] && sides[1] && sides[2])
		{
			this->transform.rot = glm::vec3(0, 0, glm::radians(180.0f));
		}

		else if (sides[1] && sides[2] && sides[3])
		{
			this->transform.rot = glm::vec3(0, 0, glm::radians(90.0f));
		}

		else if (sides[2] && sides[3] && sides[0])
		{
			this->transform.rot = glm::vec3(0, 0, glm::radians(0.0f));
		}

		else if (sides[3] && sides[0] && sides[1])
		{
			this->transform.rot = glm::vec3(0, 0, glm::radians(-90.0f));
		}

		else
		{
			this->tex = &AssetLoader::loadedTextures["road"];
			this->transform.rot = glm::vec3(0, 0, glm::radians(0.0f));
		}
	}

	//+ intersections
	else if(count == 8)
		this->tex = &AssetLoader::loadedTextures["roadCross"];
	
	max = tmp;
}

//Dtor
Road::~Road()
{

}
