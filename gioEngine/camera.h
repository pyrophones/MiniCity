#pragma once

#include "time.h"
#include "object.h"
#include <glm/gtc/matrix_transform.hpp>

extern GLfloat deltaTime;

//Class for the camera
class Camera : public Object
{
	public:
		glm::vec3 mPos;
		Camera();
		void setup(GLfloat fov, GLfloat aspectRatio, float zNear, float zFar);
		void update() override;
		glm::mat4 getCamMat() const;
		glm::mat4 getCamView() const;
		glm::mat4 getCamProj() const;
		glm::mat4 getTransMat() const;
		glm::vec3 getCamPos() const;
		~Camera() override;

	private:
		GLfloat aspect, zNear, zFar, fov;
		GLint camView, camLoc;
		glm::mat4 camMat;
		glm::mat4 view;
		glm::mat4 proj;
		glm::vec3 target;
		glm::vec3 dir;
		void render() override;
};