#include "industrial.h"

//Ctor
Industrial::Industrial(Texture* t) : Tile(t)
{
	type = TileTypes::ind;
}

//Dtor
Industrial::~Industrial()
{
	Tile::~Tile();
}
