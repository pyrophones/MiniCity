#pragma once

#include "transform.h"
#include "time.h"

//Rigidbody class for physics
class RigidBody
{
	public:
		glm::vec3 vel = glm::vec3(), force = glm::vec3();
		glm::vec3 aVel = glm::vec3(), torq = glm::vec3();
		glm::vec3 grav = glm::vec3(0, 0, 0);
		GLfloat mass = 1, speed = 1, drag = 1;
		GLfloat inert = 1, aSpeed = 1, aDrag = 1;
		GLfloat maxSpeed = 0, maxASpeed = 0;
		//GLfloat drag;
		RigidBody();
		RigidBody(Transform &t);
		void setTransform(Transform &t);
		void update();
		~RigidBody();

	private:
		Transform* t = nullptr;
};

