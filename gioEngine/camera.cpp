#include "camera.h"
#include "engine.h"

//Ctor
Camera::Camera()
{
	rigidBody.speed = 5;
	rigidBody.maxSpeed = 10;
}

//Set's the camera view and projection matricies up
void Camera::setup(GLfloat fov, GLfloat aspectRatio, float zNear, float zFar)
{
	this->fov = fov;
	this->aspect = aspectRatio;
	this->zNear = zNear;
	this->zFar = zFar;
	target = glm::vec3(0, 0, 0);

	this->transform.pos = glm::vec3(0, 0, 5);
	this->transform.rot = glm::vec3(0, 0, 0);
	this->dir = glm::normalize(this->transform.pos - target);

	view = glm::lookAt(this->transform.pos, this->transform.pos + this->transform.front, this->transform.up);
	proj = glm::perspective(this->fov, this->aspect, this->zNear, this->zFar);
	camMat = proj * view;

	camView = glGetUniformLocation(Engine::shaderMan.getProgram(), "camView");
	camLoc = glGetUniformLocation(Engine::shaderMan.getProgram(), "camLoc");
}

//Update override
void Camera::update()
{
	Object::update();

	view = glm::lookAt(this->transform.pos, this->transform.pos + this->transform.front, this->transform.up);
	proj = glm::perspective(this->fov, this->aspect, this->zNear, this->zFar);
	camMat = proj * view;

	glUniformMatrix4fv(camView, 1, GL_FALSE, glm::value_ptr(camMat));
	glUniform3fv(camLoc, 1, glm::value_ptr(this->transform.pos));
}

//Getters for cam, view, projection, and translation mats
glm::mat4 Camera::getCamMat() const
{
	return this->camMat;
}

glm::mat4 Camera::getCamView() const
{
	return this->view;
}

glm::mat4 Camera::getCamProj() const
{
	return this->proj;
}

glm::mat4 Camera::getTransMat() const
{
	return this->transform.transMat;
}

glm::vec3 Camera::getCamPos() const
{
	return this->transform.pos;
}

//Override render method
void Camera::render()
{
	Object::render();
}

//Dtor
Camera::~Camera()
{
	Object::~Object();
}