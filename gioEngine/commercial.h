#pragma once

#include "tile.h"

//Used for commercial tiles
class Commercial : public Tile
{
	public:
		Commercial(Texture* t);
		~Commercial();
};

