#pragma once

#include <sstream>
#include <vector>
#include <string>
#include <glm/glm.hpp>
#include "shader.h"
#include "texture.h"

//Used to store verticies
struct Vertex
{
	glm::vec3 vert;
	glm::vec2 uv;
	glm::vec3 norm;
};

//Loads / handels models
class Model
{
	public:
		glm::vec3 max = glm::vec3();
		glm::vec3 color = glm::vec3(1.0f, 1.0f, 1.0f);
		Model();
		Model(const GLchar* path);
		void readModel(const GLchar* path);
		bool bufferModel();
		void render(const glm::mat4 &transMat);
		void destroy();
		~Model();

	private:
		GLuint vao = 0, vbo = 0;
		GLint count = 0;
		GLint texLoc, transLoc, colorLoc;
		std::vector<Vertex> vert;
};

