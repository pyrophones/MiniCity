#include "engine.h"

//Just calls all the nessesary functions
int main()
{
	Engine engine;

	if (!engine.init())
		return -1;
	
	if (!engine.useShaders())
		return -1;

	if (!engine.bufferModels())
		return -1;

	else
		if (!engine.gameLoop())
			return -1;

		else
			return 0;
}
