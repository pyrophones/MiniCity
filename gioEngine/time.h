#pragma once

#include <GL/glew.h>

//Keeps track of dT
namespace Time
{
	extern GLfloat deltaTime, time, curTime;
	void calcDeltaTime(GLfloat newTime);
};