#include "residential.h"

//Ctor
Residential::Residential(Texture* t) : Tile(t)
{
	type = TileTypes::res;
}

//Dtor
Residential::~Residential()
{
	Tile::~Tile();
}
