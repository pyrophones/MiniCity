#pragma once

#include <iostream>
#include <vector>
#include <deque>
#define _USE_MATH_DEFINES
#include <math.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include "time.h"
#include "assetLoader.h"
#include "shader.h"
#include "object.h"
#include "camera.h"
#include "model.h"
#include "tile.h"
#include "residential.h"
#include "commercial.h"
#include "industrial.h"
#include "road.h"

static bool debug = false;
static std::deque<TileTypes> toPlace;
static TileTypes curType;
static GLint width = 800;
static GLint height = 600;

//Main engine class
class Engine
{
	public:
		static Camera cam;
		static Shader shaderMan;
		std::vector<Object> objsInScene;
	
		bool init();
		bool bufferModels();
		bool gameLoop();
		bool useShaders();
	
		~Engine();
	
	private:
		GLFWwindow* window = nullptr;
	
		glm::vec4 viewPort;
};

//Extra callbacks mainly for input and stuff
void errorCallback(GLint error, const GLchar* errorStr);

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

void mouseCallback(GLFWwindow* window, double xPos, double yPos);

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);