#pragma once

#include <iostream>
#include <GL/glew.h>
#include <FreeImage/FreeImage.h>

//Loads and uses a texture
class Texture
{
	public:
		GLint width, height, channels;
		GLfloat baseScale = 1;
		Texture();
		Texture(GLchar* path);
		void loadTexture(const GLchar* path);
		GLuint getTexture();
		void bindTexture();
		void destroy();
		~Texture();

	private:
		GLuint texture;
};