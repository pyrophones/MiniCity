#include "commercial.h"

//Ctor
Commercial::Commercial(Texture* t) : Tile(t)
{
	type = TileTypes::com;
}

//Dtor
Commercial::~Commercial()
{
	Tile::~Tile();
}
