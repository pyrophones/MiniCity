#pragma once

#include "tile.h"

//Used for industrial tiles
class Industrial : public Tile
{
	public:
		Industrial(Texture* t);
		~Industrial();
};

