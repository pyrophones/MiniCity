#pragma once

#include <map>
#include <GL/glew.h>
#include "model.h"
#include "texture.h"

// Used to load and store assets
namespace AssetLoader
{
	enum fileType { MODEL, TEXTURE };

	extern std::map<GLchar*, Model> loadedModels;
	extern std::map<GLchar*, Texture> loadedTextures;

	void load(GLchar* path, fileType type, GLchar* name);
	void unload();
};