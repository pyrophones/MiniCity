#pragma once

#include "object.h"
#include "assetLoader.h"

enum TileTypes {res, com, ind, road, bldz};

//Base class used for tiles
class Tile : public Object
{
	public:
		GLint variation;
		GLint lvl;
		GLfloat cost;
		TileTypes type;

		Tile(Texture* tex);

		virtual void update() override;

		~Tile();
};

