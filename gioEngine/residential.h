#pragma once
#include "tile.h"

//Used for residential tiles
class Residential : public Tile
{
	public:
		Residential(Texture* t);

		~Residential();
};

