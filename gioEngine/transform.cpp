#include "transform.h"

//Ctor
Transform::Transform()
{
	this->pos.x = 0;
	this->pos.y = 0;
	this->pos.z = 0;
	this->rot = glm::vec3();
}

//Ctor that accepts position values
Transform::Transform(GLfloat x, GLfloat y, GLfloat z)
{
	this->pos.x = x;
	this->pos.y = y;
	this->pos.z = z;
	this->rot = glm::vec3();
}

//Ctor that accepts position vector
Transform::Transform(glm::vec3 pos)
{
	this->pos = pos;
	this->rot = glm::vec3();
}

//Updates the transforms mat, as well as the front, right, and up vectors
void Transform::update()
{
	this->transMat = glm::translate(this->pos);
	this->transMat *= glm::scale(this->scale);
	this->transMat *= glm::yawPitchRoll(rot.x, rot.y, rot.z);

	this->front = (glm::mat3)transMat * glm::vec3(0, 0, -1);
	this->right = glm::normalize(glm::cross(front, (glm::mat3)transMat * glm::vec3(0, -1, 0)));
	this->up = glm::normalize(glm::cross(front, right));
}

//Dtor
Transform::~Transform()
{

}