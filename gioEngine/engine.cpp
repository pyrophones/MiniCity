#include "engine.h"

Camera Engine::cam;
Shader Engine::shaderMan;

//Initializes the engine
bool Engine::init()
{
	std::cout << "Starting GLFW . . ." << std::endl;

	if (!glfwInit())
	{
		std::cout << "Failed to initialize GLFW!" << std::endl;
		return false;
	}

	window = glfwCreateWindow(width, height, "MiniCity", nullptr, nullptr);

	if (window == nullptr)
	{
		std::cout << "Failed to create window pointer!" << std::endl;
		glfwTerminate();
		return false;
	}

	glfwMakeContextCurrent(window);

	glfwSetErrorCallback(errorCallback);
	glfwSetKeyCallback(window, keyCallback);
	glfwSetMouseButtonCallback(window, mouseButtonCallback);

	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW!" << std::endl;
		glfwTerminate();
		return false;
	}

	glfwGetFramebufferSize(window, &width, &height);
	glfwSetCursorPos(window, width / 2, height / 2);
	glfwSetCursorPosCallback(window, mouseCallback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	viewPort = glm::vec4(0, 0, width, height);

	glClearColor(0.4f, 0.6f, 0.9f, 1.0f);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	return true;
}

//Loads assets / buffers models
bool Engine::bufferModels()
{
	void (*load)(GLchar*, AssetLoader::fileType, GLchar*) = AssetLoader::load;

	//Models
	load("../models/quad.obj", AssetLoader::fileType::MODEL, "quad");
	load("../models/sphere.obj", AssetLoader::fileType::MODEL, "sphere");

	//Textures
	load("../textures/TestTexture.png", AssetLoader::fileType::TEXTURE, "test");
	load("../textures/dirt.png", AssetLoader::fileType::TEXTURE, "dirt");
	load("../textures/selectedTile.png", AssetLoader::fileType::TEXTURE, "selTile");
	load("../textures/road.png", AssetLoader::fileType::TEXTURE, "road");
	load("../textures/roadCross.png", AssetLoader::fileType::TEXTURE, "roadCross");
	load("../textures/roadt.png", AssetLoader::fileType::TEXTURE, "roadT");
	load("../textures/roadDiag.png", AssetLoader::fileType::TEXTURE, "roadDiag");
	load("../textures/rSelect.png", AssetLoader::fileType::TEXTURE, "rSelect"); //Selection texture for residential
	load("../textures/cSelect.png", AssetLoader::fileType::TEXTURE, "cSelect"); //Selection texture for commercial
	load("../textures/iSelect.png", AssetLoader::fileType::TEXTURE, "iSelect"); //Selection texture for industry
	load("../textures/resBuild.png", AssetLoader::fileType::TEXTURE, "resBuild"); //The actual building textures
	load("../textures/comBuild.png", AssetLoader::fileType::TEXTURE, "comBuild"); //The actual building textures
	load("../textures/indBuild.png", AssetLoader::fileType::TEXTURE, "indBuild"); //The actual building textures

	return true;
}

//Holds the main game loop
bool Engine::gameLoop()
{
	//Background wuad
	Object quad(&AssetLoader::loadedModels["quad"], &AssetLoader::loadedTextures["dirt"]);
	quad.transform.pos = glm::vec3(0, -1, 0);
	quad.transform.scale = glm::vec3(10, 10, 1);
	quad.collider = Collider::ColliderLess;

	//Currently selected tile
	Tile curTile(&AssetLoader::loadedTextures["selTile"]);
	curTile.transform.scale = glm::vec3(0.1, 0.1, 0.1);
	curTile.collider = Collider::AABB;

	//Cam setup stuff
	cam.setup(3.14159f * 0.4f, (GLfloat)width / (GLfloat)height, 0.01f, 1000.0f);
	cam.transform.pos.z = 3;
	cam.mPos.x = (GLfloat)width / 2;
	cam.mPos.y = (GLfloat)height / 2;
	cam.mod = &AssetLoader::loadedModels["sphere"];
	cam.collider = Collider::Sphere;
	
	//Light stuff
	GLint lightLoc = glGetUniformLocation(shaderMan.getProgram(), "lightLoc");
	glm::vec3 light = glm::vec3(0, 0, 15);

	shaderMan.use();
	while (!glfwWindowShouldClose(window))
	{
		//These check if the curTile should change in size or texture
		if (curType == TileTypes::road || curType == TileTypes::bldz)
		{
			curTile.tex = &AssetLoader::loadedTextures["selTile"];
			curTile.transform.scale = glm::vec3(0.05, 0.05, 0.05);
		}

		else
		{
			curTile.transform.scale = glm::vec3(0.1, 0.1, 0.1);

			if (curType == TileTypes::res)
				curTile.tex = &AssetLoader::loadedTextures["rSelect"];

			else if (curType == TileTypes::com)
				curTile.tex = &AssetLoader::loadedTextures["cSelect"];

			else if (curType == TileTypes::ind)
				curTile.tex = &AssetLoader::loadedTextures["iSelect"];
		}

		//This is used to place tiles. There's probably a better way to do it though
		while (toPlace.size() != 0)
		{
			//Only place a tile if it's not colliding
			if (!curTile.colliding)
			{
				//Res tile
				if (toPlace[0] == TileTypes::res)
				{
					Object::objectBatch.push_back(new Residential(&AssetLoader::loadedTextures["resBuild"]));
					Object::objectBatch.back()->transform.pos = curTile.transform.pos;
					Object::objectBatch.back()->transform.scale = glm::vec3(0.1, 0.1, 0.1);
					Object::objectBatch.back()->collider = Collider::AABB;
					Object::objectBatch.back()->tag = "res";
				}

				//Com tile
				else if (toPlace[0] == TileTypes::com)
				{
					Object::objectBatch.push_back(new Commercial(&AssetLoader::loadedTextures["comBuild"]));
					Object::objectBatch.back()->transform.pos = curTile.transform.pos;
					Object::objectBatch.back()->transform.scale = glm::vec3(0.1, 0.1, 0.1);
					Object::objectBatch.back()->collider = Collider::AABB;
					Object::objectBatch.back()->tag = "com";
				}

				//Ind tile
				else if (toPlace[0] == TileTypes::ind)
				{
					Object::objectBatch.push_back(new Industrial(&AssetLoader::loadedTextures["indBuild"]));
					Object::objectBatch.back()->transform.pos = curTile.transform.pos;
					Object::objectBatch.back()->transform.scale = glm::vec3(0.1, 0.1, 0.1);
					Object::objectBatch.back()->collider = Collider::AABB;
					Object::objectBatch.back()->tag = "ind";
				}

				//Road tile
				else if (toPlace[0] == TileTypes::road)
				{
					Object::objectBatch.push_back(new Road(&AssetLoader::loadedTextures["road"]));
					Object::objectBatch.back()->transform.pos = curTile.transform.pos;
					Object::objectBatch.back()->transform.scale = glm::vec3(0.05, 0.05, 0.05);
					Object::objectBatch.back()->collider = Collider::AABB;
					Object::objectBatch.back()->tag = "road";
				}
			}

			//Used for bulldozing tiles
			else if (curTile.colliding && toPlace[0] == TileTypes::bldz)
			{
				for (GLuint i = 0; i < Object::objectBatch.size(); i++)
				{
					if (curTile.collidesWith(Object::objectBatch[i]))
					{
						Object::objectBatch.erase(Object::objectBatch.begin() + i);
						i--;
					}
				}
			}

			toPlace.pop_front();
		}
		
		//Convert screen coords to world for mouse
		glm::vec3 t = glm::unProject(cam.mPos, cam.getCamView(), cam.getCamProj(), viewPort);
		curTile.transform.pos.x =  ceilf(t.x * 20) / 20;
		curTile.transform.pos.y = -ceilf(t.y * 20) / 20;
		curTile.transform.pos.z = 1;

		//Get dT
		Time::calcDeltaTime((GLfloat)glfwGetTime());
		glfwPollEvents();

		//Update cam
		cam.update();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		//Update each object
		for (auto i : Object::objectBatch)
		{
			i->update();
		}
		
		glUniform3fv(lightLoc, 1, glm::value_ptr(light));

		//Render each object
		for (auto i : Object::objectBatch)
		{
			i->render();
		}

		glfwSwapBuffers(window);
	}

	return true;
}

//Makes / uses the shaders
bool Engine::useShaders()
{
	if (!shaderMan.loadShaders("../gioEngine/vert.glsl", "../gioEngine/frag.glsl"))
	{
		std::cin.get();
		return false;
	}

	else
		return true;
}

//Engine Dtor calls AssetLoader::unload()
Engine::~Engine()
{	
	AssetLoader::unload();

	glfwTerminate();
}

//Callbacks

void errorCallback(GLint error, const GLchar* errorStr)
{
	std::cout << "GLFW error: " << error << errorStr << std::endl;
}

//Used for key input
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Camera* cam = &Engine::cam;
	
	//Switchs movement keys when in debug mode
	if (debug)
	{
		if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT))
			cam->rigidBody.force += (glm::mat3)cam->getTransMat() * glm::vec3(0, 0, -5);

		if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT))
			cam->rigidBody.force += (glm::mat3)cam->getTransMat() * glm::vec3(0, 0, 5);
	}

	else
	{
		if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT))
			cam->rigidBody.force += (glm::mat3)cam->getTransMat() * glm::vec3(0, 5, 0);

		if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT))
			cam->rigidBody.force += (glm::mat3)cam->getTransMat() * glm::vec3(0, -5, 0);
	}

	//Game play keys
	if (key == GLFW_KEY_RIGHT && (action == GLFW_PRESS || action == GLFW_REPEAT))
		cam->rigidBody.force += (glm::mat3)cam->getTransMat() * glm::vec3(5, 0, 0);

	if (key == GLFW_KEY_LEFT && (action == GLFW_PRESS || action == GLFW_REPEAT))
		cam->rigidBody.force += (glm::mat3)cam->getTransMat() * glm::vec3(-5, 0, 0);

	if (key == GLFW_KEY_Q && action == GLFW_PRESS)
		curType = TileTypes::res;

	if (key == GLFW_KEY_A && action == GLFW_PRESS)
		curType = TileTypes::com;

	if (key == GLFW_KEY_Z && action == GLFW_PRESS)
		curType = TileTypes::ind;

	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		curType = TileTypes::road;

	if (key == GLFW_KEY_B && action == GLFW_PRESS)
		curType = TileTypes::bldz;

	//Toggle debug mode
	if (key == GLFW_KEY_GRAVE_ACCENT && action == GLFW_PRESS)
		debug = !debug;

	//Quit
	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

//Mouse movement callback
void mouseCallback(GLFWwindow* window, double xPos, double yPos)
{
	Camera* cam = &Engine::cam;

	glm::vec2 offset(cam->mPos.x - xPos, cam->mPos.y - yPos);

	//Set cams mouse position and get a z
	cam->mPos.x = (GLfloat)xPos;
	cam->mPos.y = (GLfloat)yPos;

	glReadPixels(cam->mPos.x, cam->mPos.y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &cam->mPos.z);

	//Only allow 3D movement in debug mode
	if (debug)
	{
		cam->transform.rot.x += offset.x * 0.5f * Time::deltaTime;
		cam->transform.rot.y += offset.y * 0.5f * Time::deltaTime;
	}
}

//Mouse button callback
void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	//Push back the current tile type
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		toPlace.push_back(curType);
	}
}