#include "assetLoader.h"

std::map<GLchar*, Model> AssetLoader::loadedModels; //Holds all loaded models
std::map<GLchar*, Texture> AssetLoader::loadedTextures; //Holds all loaded textures

//Load method for assets
void AssetLoader::load(GLchar* path, fileType type, GLchar* name)
{
	//Check the file type to load properly
	if (type == fileType::MODEL)
	{
		Model tmp(path);
		tmp.bufferModel();
		loadedModels.insert(std::pair<GLchar*, Model>(name, tmp));
	}

	else if (type == fileType::TEXTURE)
	{
		loadedTextures.insert(std::pair<GLchar*, Texture>(name, Texture(path)));
	}
}

//Properly unloads assets
void AssetLoader::unload()
{
	for (auto i : loadedModels)
	{
		i.second.destroy();
	}

	loadedModels.clear();

	for (auto i : loadedTextures)
	{
		i.second.destroy();
	}

	loadedTextures.clear();
}