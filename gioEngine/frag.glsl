#version 430 core 

in vec3 VERT;
in vec2 UV;
in vec3 NORM;
uniform sampler2D tex;
uniform vec3 texColor;
uniform vec3 lightLoc;
uniform vec3 camLoc;

void main()
{
	vec3 l = normalize(lightLoc - VERT);
	vec3 v = normalize(camLoc - VERT);
	vec3 h = normalize(l + v);
	vec3 n = normalize(NORM);

	float amb = 0.1;
	float diff = max(dot(l, n), 0);
	float alpha = 16;
	float spec = pow(max(dot(h, n), 0), alpha);

	vec3 color = texColor * (amb + diff + spec);

	gl_FragColor = texture(tex, UV) * vec4(color, 1.0);
}