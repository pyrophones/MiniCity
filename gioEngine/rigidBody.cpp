#include "rigidBody.h"

//Ctor
RigidBody::RigidBody()
{

}

//Ctor that loads a transform
RigidBody::RigidBody(Transform &t)
{
	this->t = &t;
}

//Sets the rigidbodies transform
void RigidBody::setTransform(Transform &t)
{
	this->t = &t;
}

//Update calculateds angular and linear velocity
void RigidBody::update()
{
	this->torq += -aVel * drag;
	this->force += -vel * drag;
	this->force += grav;

	/* Angular */
	glm::vec3 aA = this->torq / this->inert;
	aVel = aA * Time::deltaTime;

	//Check for max angular speed
	if (maxASpeed != 0)
	{
		if (this->aVel.length() > maxASpeed)
			this->aVel = glm::normalize(this->aVel) * maxASpeed;
	}

	this->t->rot += aVel * aSpeed * Time::deltaTime;

	/* Linear */
	glm::vec3 a = this->force / this->mass;
	vel = a * Time::deltaTime;

	//Check when to zero the velocity
	if ((this->vel.x < 0.05 && this->vel.x > -0.05) && (this->vel.y < 0.05  && this->vel.y > -0.05) && (this->vel.z < 0.05  && this->vel.z > -0.05))
		this->vel = glm::vec3();

	//Check for max speed
	if (maxSpeed != 0)
	{
		if (this->vel.length() > maxSpeed)
			this->vel = glm::normalize(this->vel) * maxSpeed;
	}

	this->t->pos += vel * speed * Time::deltaTime;
}

RigidBody::~RigidBody()
{

}
