#pragma once

#include <vector>
#include "texture.h"
#include "model.h"
#include "transform.h"
#include "rigidBody.h"

enum Collider { ColliderLess, AABB, Sphere };

//Object Class
class Object
{
	public:
		bool zCollisions = true;
		static std::vector<Object*> objectBatch; // std::vector of the objects
		Collider collider;
		RigidBody rigidBody;
		GLchar* tag = nullptr;
		Texture* tex = nullptr;
		Model* mod = nullptr;
		Transform transform;
		bool colliding = false;

		Object();
		Object(Model* m);
		Object(Texture* t);
		Object(Model* m, Texture* t);

		virtual void update();
		virtual void render();
		bool collidesWith(const Object* obj);
		virtual ~Object();

	protected:
		glm::vec3 max;
};